package lock;

import java.util.Arrays;
import java.util.Random;

public class ComboLock implements Lock 
{
	public static final int MAX_NUMBER = 50;
	private int[] myCombination;
	private int[] myAttempt;
	private boolean myIsLocked;
	private boolean myIsReset;

	public ComboLock() 
	{	
		myIsLocked = true;
		myIsReset = false;
		
		Random num = new Random();
		myCombination = new int[3];
		myCombination[0] = Math.abs(num.nextInt(MAX_NUMBER));
		myCombination[1] = Math.abs(num.nextInt(MAX_NUMBER));
		myCombination[2] = Math.abs(num.nextInt(MAX_NUMBER));

		int one = myCombination[0] % 4;
		int two = myCombination[1] % 4;
		int three = myCombination[2] % 4;

	    while(three != one)
		{
			myCombination[2] = Math.abs(num.nextInt(MAX_NUMBER));
			three = myCombination[2] % 4;
		}

		while(two != (one + 2) % 4)
		{
			myCombination[1] = Math.abs(num.nextInt(MAX_NUMBER));
			two = myCombination[1] % 4;
		}
		
		myAttempt = new int[3];
//		myAttempt[0] = -1;
//		myAttempt[1] = -1;
//		myAttempt[2] = -1;
	}

	public void turnRight(int number) 
	{

		if(number == MAX_NUMBER)
		{
			myAttempt[0] = -1;
		}
	    else if(number >= 0 && number < MAX_NUMBER && myAttempt[0] == -1)
	    {
	        myAttempt[0] = number;
	    }
		else if( myAttempt[0] != -1)
		{
			myAttempt[2] = number;
		}
	}

	public void turnLeft(int number) 
	{
		myAttempt[1] = number;
	}

	public void reset() 
	{
		myIsLocked = true;
		myIsReset = true;
		
//		myAttempt[0] = -1;
//		myAttempt[1] = -1;
//		myAttempt[2] = -1;
	}

	public int[] getCombination() 
	{
		return myCombination;
	}

	public boolean getIsReset() 
	{
		return myIsReset;
	}

	public boolean isLocked() 
	{
		boolean areEqual = Arrays.equals(myCombination, myAttempt);
		if(areEqual == true)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	public boolean lock() 
	{
		myIsLocked = true;
		return myIsLocked;
	}

	public boolean unlock() 
	{
		boolean areEqual = Arrays.equals(myCombination, myAttempt);
		if(areEqual == false || lock() == true )
		{
			return false;
		}
		else
		{
			return true;
		}
	}
}
