package lock;

import java.util.Random;

public class KeylessEntryLock extends KeyLock 
{
	public static final int MAX_NUM_USER_CODES = 10;
	public static final int USER_CODE_LENGTH = 4;
	public static final int MASTER_CODE_LENGTH = 6;

	private boolean myIsReset;
	private boolean myIsNewUserCode;
	private boolean myIsDeletedUserCode;
	private boolean myIsChangedMasterCode;
	private boolean myAllUserCodesDeleted;
	private int[] myMasterCode;
	private int[][] myUserCodes;
	private int[] myDefaultMasterCode;
	
	private int[] myAttempt;

	int i = 0;
//	boolean valid;
	String pushing;
	
	public KeylessEntryLock(int keyValue) 
	{
		super(keyValue);
		this.myKey = keyValue;

		Random num = new Random();
		myDefaultMasterCode = new int[MASTER_CODE_LENGTH];
		
		for(int i = 0; i < MASTER_CODE_LENGTH; i++)
		{
			myDefaultMasterCode[i] = Math.abs(num.nextInt(MAX_NUM_USER_CODES));
		}

		myMasterCode = new int[MASTER_CODE_LENGTH];
		myAttempt = new int[USER_CODE_LENGTH];
		myUserCodes = new int[MAX_NUM_USER_CODES][USER_CODE_LENGTH];
	}

	public boolean pushKey(char key) 
	{
		pushing += key;
		if(pushing.length() == 16 && pushing.substring(7) == "1")
		{	
			addedUserCode();
			pushing = null;
		}
		else if(pushing.length() == 16 && pushing.substring(7) == "2")
		{
			deletedUserCode();
			pushing = null;
		}
		else if(pushing.length() == 14 && pushing.substring(7) == "6")
		{
			clearedAllUserCodes();
			pushing = null;
		}
		else if(pushing.length() == 20 && pushing.substring(7) == "3")
		{
			changedMasterCode();
			pushing = null;
		}
		else if(pushing.length() == 4)
		{
			myAttempt[0] = Integer.parseInt(pushing.substring(0));
			myAttempt[1] = Integer.parseInt(pushing.substring(1));
			myAttempt[2] = Integer.parseInt(pushing.substring(2));
			myAttempt[3] = Integer.parseInt(pushing.substring(3));
			
			pushing = null;
		}
		return true;
	}

	public boolean addedUserCode() 
	{ 
		if(pushing.substring(8,11) == pushing.substring(12,14))
		{	
			myAttempt[0] = Integer.parseInt(pushing.substring(8));
			myAttempt[1] = Integer.parseInt(pushing.substring(9));
			myAttempt[2] = Integer.parseInt(pushing.substring(10));
			myAttempt[3] = Integer.parseInt(pushing.substring(11));
				
			for(int j = 0; j < USER_CODE_LENGTH; j++)
			{
				if(myUserCodes[i][0] == 0)
				{
					myUserCodes[i][j] = myAttempt[j];
					i++;
				}
			}
			myIsNewUserCode = true;
		}
		else
		{
			myIsNewUserCode = false;
		}		
		return myIsNewUserCode;
	}

	public boolean deletedUserCode() 
	{
		if(pushing.substring(8,11) == pushing.substring(12,14))
		{
			myAttempt[0] = Integer.parseInt(pushing.substring(8));
			myAttempt[1] = Integer.parseInt(pushing.substring(9));
			myAttempt[2] = Integer.parseInt(pushing.substring(10));
			myAttempt[3] = Integer.parseInt(pushing.substring(11));
			for(int r = 0; r < i; r++)
			{
				for(int j = 0; j < USER_CODE_LENGTH; j++)
				{
	
					if(myUserCodes[r][j] == myAttempt[j])
					{
						myUserCodes[r][j] = 0;
					}
				}
			}
			myIsDeletedUserCode = true;
		}
		else
		{
			myIsDeletedUserCode = false;	
		}
		return myIsDeletedUserCode;
	}

	public boolean clearedAllUserCodes() 
	{
		if(pushing.substring(0,5) == pushing.substring(8,13))
		{
			for(int r = 0; r < i; r++)
			{
				for(int j = 0; j < USER_CODE_LENGTH; j++)
				{
					myUserCodes[r][j] = 0;
				}
			}
			myAllUserCodesDeleted = true;
		}
		else
		{
			myAllUserCodesDeleted = false;
		}
		return myAllUserCodesDeleted;
	}

	public boolean changedMasterCode() 
	{

		if(pushing.substring(8,13) == pushing.substring(14,19))
		{
			myMasterCode[0] = Integer.parseInt(pushing.substring(8));
			myMasterCode[1] = Integer.parseInt(pushing.substring(9));
			myMasterCode[2] = Integer.parseInt(pushing.substring(10));
			myMasterCode[3] = Integer.parseInt(pushing.substring(11));
			myMasterCode[4] = Integer.parseInt(pushing.substring(12));
			myMasterCode[5] = Integer.parseInt(pushing.substring(13));
			
	        myDefaultMasterCode = myMasterCode;
	        myIsChangedMasterCode = true;
		}
		else
		{
			myIsChangedMasterCode = false;	
		}
		return myIsChangedMasterCode;
	}

	public int[] getDefaultMasterCode()
	{
		return myDefaultMasterCode;
	}

	public boolean isLocked() 
	{
		for(int i = 0; i< MAX_NUM_USER_CODES; i++)
		{
			for(int j= 0; j< USER_CODE_LENGTH; j++)
			{
				if(myUserCodes[i][j] != myAttempt[j])
				{
					myIsLocked= true;
				}
				else
				{
					myIsLocked = false;
					break;
				}
			}
		}
		return myIsLocked;
	}

	public boolean lock() 
	{
		return false;
	}

	public boolean unlock() 
	{
		for(int i = 0; i< MAX_NUM_USER_CODES; i++)
		{
			for(int j= 0; j< USER_CODE_LENGTH; j++)
			{

				if(myUserCodes[i][j] != myAttempt[j])
				{
					myIsLocked= true;
				}
				else
				{
					myIsLocked = false;
					break;
				}
			}
		}
		return myIsLocked;
	}
}
