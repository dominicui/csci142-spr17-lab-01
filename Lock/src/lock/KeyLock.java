package lock;

public class KeyLock implements Lock 
{
	protected int myKey;
	protected boolean myIsLocked;
	private boolean myIsInserted;

	public KeyLock(int keyValue) 
	{
		myKey = keyValue;
		myIsLocked = true;
		myIsInserted = false;
	}

	public void insert(int keyValue) 
	{
		if (myKey == keyValue) 
		{
			myIsInserted = true;
		} 
		else 
		{
			myIsInserted = false;
		}
	}

	public boolean turn() 
	{
		if (myIsInserted == true) 
		{
			myIsLocked = false;
			return true;
		} 
		else
		{
			myIsLocked = true;
			return false;
		}
	}

	public boolean isLocked() 
	{
		if (myIsInserted == false || myIsLocked == true) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public boolean lock() 
	{
		return myIsLocked = true;
	}

	public boolean unlock() 
	{
		if (myIsLocked == false) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
